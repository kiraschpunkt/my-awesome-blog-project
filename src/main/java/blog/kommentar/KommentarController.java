package blog.kommentar;

import blog.beitrag.Beitrag;
import blog.beitrag.BeitragService;
import blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class KommentarController {

    @Autowired
    private KommentarService kommentarService;
    @Autowired
    private BeitragService beitragService;

    @PostMapping("/kommentieren")
    public String addKommentar(@Valid @ModelAttribute KommentarDTO kommentarDTO, BindingResult bindingResult,
                                @ModelAttribute("sitzungNutzer") Nutzer nutzer,
                                @RequestParam(value = "beitragsId") Integer beitragsId) {

        if (bindingResult.hasErrors()) {
            return "kommentarDummkopf";
        }

        Optional<Beitrag> optionalBeitrag = beitragService.getBeitrag(beitragsId);

        if (!optionalBeitrag.isPresent()) {
            return "redirect:/";
        }

        Kommentar kommentar = new Kommentar(nutzer.getName(), kommentarDTO.getTextKommentar(), optionalBeitrag.get());
        kommentarService.addKommentar(kommentar);
        return "redirect:/";
    }

    @PostMapping("/deleteKommentar")
    public String deleteKommentar(@RequestParam(value = "deleteKommentar") Integer id) {

        Optional<Kommentar> optionalKommentar = kommentarService.getKommentar(id);

        optionalKommentar.ifPresent(kommentar -> kommentarService.deleteKommentar(kommentar));

        return "redirect:/";
    }

}
