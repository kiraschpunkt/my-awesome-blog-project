package blog.kommentar;

import blog.beitrag.Beitrag;
import blog.nutzer.Nutzer;

import javax.validation.constraints.NotEmpty;

public class KommentarDTO {

    //ToDo: Check, which attributes you actually use.

    private int id;
    private Beitrag beitrag;
    private Nutzer nutzer;
    @NotEmpty
    private String textKommentar;

    public KommentarDTO() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public String getTextKommentar() {
        return textKommentar;
    }

    public void setTextKommentar(String textKommentar) {
        this.textKommentar = textKommentar;
    }
}
