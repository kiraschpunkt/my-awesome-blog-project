package blog.kommentar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KommentarService {

    @Autowired
    KommentarRepository kommentarRepository;

    public void addKommentar(Kommentar kommentar) {
        kommentarRepository.save(kommentar);
    }

    public void deleteKommentar(Kommentar kommentar) {
        kommentarRepository.delete(kommentar);
    }

    public Optional<Kommentar> getKommentar(int id) {
        return kommentarRepository.findById(id);
    }
}
