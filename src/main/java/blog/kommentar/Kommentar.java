package blog.kommentar;

import blog.beitrag.Beitrag;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    private String Nutzer;
    @Column(columnDefinition = "TEXT")
    private String textKommentar;
    private LocalDateTime kommentarZeit;

    @ManyToOne
    private Beitrag beitrag;

    public Kommentar() {
        this.kommentarZeit = LocalDateTime.now();
    }

    public Kommentar(String nutzer, String textKommentar, Beitrag beitrag) {
        Nutzer = nutzer;
        this.textKommentar = textKommentar;
        this.beitrag = beitrag;
        this.kommentarZeit = LocalDateTime.now();
    }

    public int getId() {
        return Id;
    }

    public String getNutzer() {
        return Nutzer;
    }

    public String getTextKommentar() {
        return textKommentar;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }
}
