package blog.session;

import blog.nutzer.Nutzer;
import blog.nutzer.NutzerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class SitzungsService {

    @Autowired
    private NutzerRepository nutzerRepository;

    @Autowired
    private SitzungsRepository sitzungsRepository;

    public Optional<Nutzer> getNutzerByIdPasswort(String name, String passwort) {
       return nutzerRepository.findByNameAndPasswort(name, passwort);
    }

    public void addSitzung(Sitzung sitzung) {
        sitzungsRepository.save(sitzung);
    }

    public Optional<Sitzung> getSitzung(String sessionId, Instant instant) {
       return sitzungsRepository.findByIdAndEndetInAfter(sessionId, instant);
    }

    public void delete(Sitzung sitzung) {
        sitzungsRepository.delete(sitzung);
    }
}
