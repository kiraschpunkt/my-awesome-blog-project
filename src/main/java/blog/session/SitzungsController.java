package blog.session;

import blog.nutzer.Nutzer;
import blog.nutzer.NutzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SitzungsController {

    @Autowired
    private SitzungsService sitzungsService;
    @Autowired
    private NutzerService nutzerService;


    @PostMapping("/login")
    public String einloggen(@Valid @ModelAttribute("loginDTO") LoginDTO loginDTO, BindingResult bindingResult,
                            HttpServletResponse response) {
        Optional<Nutzer> optionalNutzer = sitzungsService.getNutzerByIdPasswort(loginDTO.getName(), loginDTO.getPasswort());

        if (optionalNutzer.isPresent()) {
            Sitzung sitzung = new Sitzung(optionalNutzer.get(), Instant.now().plusSeconds(60 * 60 * 24));
            sitzungsService.addSitzung(sitzung);

            Cookie cookie = new Cookie("sitzungsId", sitzung.getId());
            response.addCookie(cookie);

            return "redirect:/";
        }

        bindingResult.addError(new FieldError("loginDTO", "passwort", ""));
        return "loginDummkopf";
    }

    @PostMapping("/logout")
    public String logout(@CookieValue(value = "sitzungsId", defaultValue = "") String sitzungsId,
                         HttpServletResponse response) {
        Optional<Sitzung> optionalSession = sitzungsService.getSitzung(sitzungsId,
                Instant.now());
        optionalSession.ifPresent(sitzung -> sitzungsService.delete(sitzung));

        Cookie cookie = new Cookie("sitzungsId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        return "redirect:/";
    }
}
