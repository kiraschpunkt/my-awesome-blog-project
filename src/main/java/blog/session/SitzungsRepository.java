package blog.session;

import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.Optional;

public interface SitzungsRepository extends CrudRepository<Sitzung, Integer> {
    Optional<Sitzung> findByIdAndEndetInAfter(String id, Instant endetIn);

}
