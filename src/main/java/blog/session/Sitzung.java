package blog.session;

import blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Sitzung {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private Nutzer nutzer;

    private Instant endetIn;

    public Sitzung() {}

    public Sitzung(Nutzer nutzer, Instant endetIn) {
        this.nutzer = nutzer;
        this.endetIn = endetIn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public Instant getEndetIn() {
        return endetIn;
    }

    public void setEndetIn(Instant endetIn) {
        this.endetIn = endetIn;
    }
}
