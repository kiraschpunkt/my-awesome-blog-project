package blog.session;

import blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.Instant;
import java.util.Optional;

@ControllerAdvice
public class SitzungsControllerAdvice {

    @Autowired
    private SitzungsService sitzungsService;


    @ModelAttribute("sitzungNutzer")
    public Nutzer sitzungNutzer(@CookieValue(value = "sitzungsId", defaultValue = "") String sitzungsId) {
        if(!sitzungsId.isEmpty()) {
            Optional<Sitzung> optionalSitzung = sitzungsService.getSitzung(sitzungsId,Instant.now());
            if (optionalSitzung.isPresent()) {
                Sitzung sitzung = optionalSitzung.get();

                sitzung.setEndetIn(Instant.now().plusSeconds(24*60*60));
                sitzungsService.addSitzung(sitzung);

                return sitzung.getNutzer();
            }
        }
        return null;
    }
}
