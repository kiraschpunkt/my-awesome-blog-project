package blog;

import blog.beitrag.BeitragDTO;
import blog.beitrag.BeitragService;
import blog.kommentar.KommentarDTO;
import blog.nutzer.Nutzer;
import blog.nutzer.NutzerService;
import blog.nutzer.AdminDTO;
import blog.session.LoginDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class MainController {

    @Autowired
    private BeitragService beitragService;
    @Autowired
    private NutzerService nutzerService;

    @GetMapping("/")
    public String index(@ModelAttribute("sitzungNutzer") Nutzer sitzungNutzer, Model model) {

        model.addAttribute("beitragsListe", beitragService.getBeitragsListe());
        model.addAttribute("loginDTO", new LoginDTO());
        model.addAttribute("kommentarDTO", new KommentarDTO());
        model.addAttribute("nutzerListe", nutzerService.getListeKeinAdmin());
        model.addAttribute("adminDTO", new AdminDTO());
        model.addAttribute("beitragDTO", new BeitragDTO());

        return "index";
    }

}
