package blog.beitrag;

import blog.kommentar.Kommentar;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String autor;

    @Column(columnDefinition = "TEXT")
    private String textBeitrag;

    private String thema;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;

    private LocalDateTime beitragZeit;

    public Beitrag() {
        this.beitragZeit = LocalDateTime.now();
    }


    public Beitrag(String autor, String thema, String textBeitrag) {
        this.autor = autor;
        this.thema = thema;
        this.textBeitrag = textBeitrag;
        this.beitragZeit = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getThema() {
        return thema;
    }

    public void setThema(String thema) {
        this.thema = thema;
    }

    public String getTextBeitrag() {
        return textBeitrag;
    }

    public void setTextBeitrag(String textBeitrag) {
        this.textBeitrag = textBeitrag;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public String getBeitragZeit() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy - hh:mm");
        return formatter.format(this.beitragZeit);
    }
}
