package blog.beitrag;

import blog.kommentar.Kommentar;
import blog.kommentar.KommentarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BeitragService {

    private BeitragsRepository beitragsRepository;
    private KommentarRepository kommentarRepository;

    public BeitragService(BeitragsRepository beitragsRepository, KommentarRepository kommentarRepository) {
        this.beitragsRepository = beitragsRepository;
        this.kommentarRepository = kommentarRepository;
    }

    public List<Beitrag> getBeitragsListe() {
        return beitragsRepository.findAllByOrderByIdDesc();
    }

    public Optional<Beitrag> getBeitrag(Integer id) {
        return beitragsRepository.findById(id);
    }

    public void addBeitrag(Beitrag beitrag) {
        beitragsRepository.save(beitrag);
    }

    public void deleteBeitrag(int id) {
        List<Kommentar> kommentare = kommentarRepository.findAllByBeitrag_Id(id);
        for (Kommentar kommentar : kommentare) {
            kommentarRepository.delete(kommentar);
        }
        beitragsRepository.deleteById(id);
    }
}
