package blog.beitrag;

import blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import java.util.Optional;

@Controller
public class BeitragController {

    @Autowired
    private BeitragService beitragService;

    @PostMapping("/addBeitrag")
    public String addBeitrag(@Valid @ModelAttribute BeitragDTO beitragDTO, BindingResult bindingResult,
                             @ModelAttribute("sitzungNutzer") Nutzer nutzer) {

        if (bindingResult.hasErrors()) {
            return "beitragDummkopf";
        }
        Beitrag beitrag = new Beitrag(nutzer.getName(), beitragDTO.getThema(), beitragDTO.getTextBeitrag());
        beitragService.addBeitrag(beitrag);
        return "redirect:/";
    }

    @GetMapping("/editBeitrag")
    public String editBeitrag(@RequestParam(value = "editBeitrag") Integer id, Model model) {
        Optional<Beitrag> optionalBeitrag = beitragService.getBeitrag(id);
        model.addAttribute("beitragDTO", new BeitragDTO(id, optionalBeitrag.get().getTextBeitrag(), optionalBeitrag.get().getThema()));
        return "editBeitrag";
    }

    @PostMapping("/editBeitrag")
    public String editBeitrag(@Valid @ModelAttribute("beitragDTO") BeitragDTO beitragDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "beitragDummkopf";
        }
        Optional<Beitrag> optionalBeitrag = beitragService.getBeitrag(beitragDTO.getId());

        Beitrag bearbeitetBeitrag = optionalBeitrag.get();
        bearbeitetBeitrag.setThema(beitragDTO.getThema());
        bearbeitetBeitrag.setTextBeitrag(beitragDTO.getTextBeitrag());
        beitragService.addBeitrag(bearbeitetBeitrag);

        return "redirect:/";
    }

    @PostMapping("/deleteBeitrag")
    public String deleteBeitrag(@RequestParam(value = "deleteBeitrag") Integer id) {

        beitragService.deleteBeitrag(id);

        return "redirect:/";
    }
}
