package blog.beitrag;

import javax.validation.constraints.NotEmpty;

public class BeitragDTO {

    private int id;

    private String autor;

    @NotEmpty
    private String textBeitrag;
    @NotEmpty
    private String thema;

    public BeitragDTO() {
    }

    public BeitragDTO(int id, String textBeitrag, String thema) {
        this.textBeitrag = textBeitrag;
        this.thema = thema;
        this.id = id;
    }

    public String getTextBeitrag() {
        return textBeitrag;
    }

    public String getThema() {
        return thema;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setTextBeitrag(String textBeitrag) {
        this.textBeitrag = textBeitrag;
    }

    public void setThema(String thema) {
        this.thema = thema;
    }
}
