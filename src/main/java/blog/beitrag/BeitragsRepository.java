package blog.beitrag;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BeitragsRepository extends CrudRepository<Beitrag, Integer> {

    List<Beitrag> findAllByOrderByIdDesc();

    void deleteById(int id);
}
