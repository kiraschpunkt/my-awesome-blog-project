package blog.nutzer;

import blog.session.Sitzung;
import blog.session.SitzungsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.Instant;

@Controller
public class RegistrierungsController {

    @Autowired
    private NutzerService nutzerService;
    @Autowired
    private SitzungsService sitzungsService;

    @GetMapping("/registrierung")
    public String getRegistrierung(Model model) {
        model.addAttribute("nutzer", new RegistrierungsDTO("", "", ""));
        return "registrierung";
    }

    @PostMapping("/registrierung")
    public String registrieren(@Valid @ModelAttribute("nutzer") RegistrierungsDTO registrierungsDTO, BindingResult bindingResult, HttpServletResponse response) {

        if (!registrierungsDTO.getPasswort().equals(registrierungsDTO.getPasswort2())) {
            bindingResult.addError(new FieldError("nutzer", "passwort2", "Passwörter müssen übereinstimmen."));
        }

        if (nutzerService.checkNutzer(registrierungsDTO)) {
            bindingResult.addError(new FieldError("nutzer", "name", "Benutzername existiert bereits."));
        }

        if (bindingResult.hasErrors()) {
                return "registrierung";
            }
        Nutzer nutzer = new Nutzer();
        nutzer.setName(registrierungsDTO.getName());
        nutzer.setPasswort(registrierungsDTO.getPasswort());
        nutzerService.addNutzer(nutzer);

        Sitzung sitzung = new Sitzung(nutzer, Instant.now().plusSeconds(60 * 60 * 24));
        sitzungsService.addSitzung(sitzung);

        Cookie cookie = new Cookie("sitzungsId", sitzung.getId());
        response.addCookie(cookie);
        return "redirect:/";
    }

}
