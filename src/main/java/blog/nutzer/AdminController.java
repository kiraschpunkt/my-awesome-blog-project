package blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AdminController {

    @Autowired
    private NutzerService nutzerService;

    @PostMapping("/addAdmin")
    public String addAdmin(@ModelAttribute("adminDTO") AdminDTO adminDTO, Model model) {

        Nutzer nutzer = nutzerService.getNutzerById(adminDTO.getId()).get();

        nutzer.setRolle("admin");
        nutzerService.addNutzer(nutzer);

        model.addAttribute("neuerAdmin", nutzer);

        return "bestaetigungAdmin";
    }
}
