package blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NutzerService {

    @Autowired
    private NutzerRepository nutzerRepository;

    public void addNutzer(Nutzer nutzer) {
        nutzerRepository.save(nutzer);
    }

    public boolean checkNutzer(RegistrierungsDTO registrierungsDTO) {
       return nutzerRepository.existsByName(registrierungsDTO.getName());
    }


    public Optional<Nutzer> getNutzerById(int id) {
        return nutzerRepository.findById(id);
    }

    public List<Nutzer> getListeKeinAdmin() {
        return nutzerRepository.findByRolle("nutzer");
    }
}
