package blog.nutzer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NutzerRepository extends CrudRepository<Nutzer, Integer> {
    boolean existsByName(String name);
    Optional<Nutzer> findByNameAndPasswort(String name, String passwort);
    List<Nutzer> findByRolle(String rolle);
}
