package blog.nutzer;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegistrierungsDTO {

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9_]+$", message = "Der Username darf nur Buchstaben und Zahlen enthalten.")
    private String name;

    @Size(min = 6, message = "Das Passwort muss mindestens 6 Zeichen lang sein.")
    private String passwort;
    private String passwort2;

    public RegistrierungsDTO(String name, String passwort, String passwort2) {
        this.name = name;
        this.passwort = passwort;
        this.passwort2 = passwort2;
    }

    public String getName() {
        return name;
    }

    public String getPasswort() {
        return passwort;
    }

    public String getPasswort2() {
        return passwort2;
    }
}
